#!/usr/bin/env python

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

# -*- coding: utf-8 -*-

"""The setup script."""

import os
from setuptools import find_packages, setup


def normalize(package):
    if package.startswith("."):
        p = package.split("/")[-1]
        return p + "@file://" + os.path.normpath(os.getcwd() + "/" + package)

    return package


with open("README.rst") as readme_file:
    readme = readme_file.read()

with open("requirements/base.txt") as f:
    requirements = [normalize(line) for line in f.read().splitlines()]

with open("requirements/test.txt") as f:
    test_requirements = [normalize(line) for line in f.read().splitlines()]

setup(
    author="Blessy Maria Xavier",
    author_email="blessy@mintlab.nl",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: European Union Public Licence 1.2 (EUPL 1.2)",
        "Natural Language :: English",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
    ],
    description="Some repo does things",
    setup_requires=["pytest-runner"],
    install_requires=requirements,
    dependency_links=[],
    license="EUPL license",
    long_description=readme,
    include_package_data=True,
    keywords="wopi_proxy",
    name="wopi_proxy",
    packages=find_packages(include=["wopi_proxy", "wopi_proxy.*"]),
    package_data={"": ["*.json"]},
    test_suite="tests",
    tests_require=test_requirements,
    extras_require={},
    url="https://gitlab.com/minty-python/wopi-proxy",
    version="0.0.3",
    zip_safe=False,
    entry_points={"paste.app_factory": ["main = wopi_proxy:main"]},
)
