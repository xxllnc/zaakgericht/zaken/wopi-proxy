# WOPI Proxy

This is a helm chart for installing the wopi-proxy: https://gitlab.com/minty-python/wopi-proxy/-/tree/master

## Local rendering

```
helm template --debug wopi-proxy ./wopi-proxy --values wopi-proxy/values.yaml --values wopi-proxy/values-test.yaml
```
