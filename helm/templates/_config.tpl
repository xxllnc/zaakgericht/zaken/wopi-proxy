# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

{{/*
    wopi-proxy statsd
*/}}

{{- define "wopi-proxy.statsd.host" -}}
{{- default "localhost" .Values.externalStatsd.host -}}
{{- end -}}

{{- define "wopi-proxy.statsd.port" -}}
{{- default "8125" .Values.externalStatsd.port -}}
{{- end -}}

{{/*
    wopi-proxy logging
*/}}

{{- define "wopi-proxy.logging.conf" -}}
[loggers]
keys = root, wopi_proxy

[handlers]
keys = console

[formatters]
keys = generic

[logger_root]
level = INFO
handlers = console

[logger_wopi_proxy]
level = DEBUG
handlers =
qualname = wopi_proxy

[handler_console]
class = StreamHandler
args = (sys.stderr,)
level = NOTSET
formatter = generic

[formatter_generic]
format = %(asctime)s %(levelname)-5.5s [%(name)s:%(lineno)s][%(threadName)s] %(message)s
{{- end -}}


{{/*
    wopi-proxy service.conf
*/}}

{{- define "wopi-proxy.service.conf" -}}
<InstanceConfig>
    type = none
</InstanceConfig>

<redis>
  <session>
    host     = {{ required "Missing: .Values.redisHost!" .Values.redisHost | quote }}
    port     = {{ required "Missing: .Values.redisPort!" .Values.redisPort | quote }}
    ssl      = 1
    db       = {{ required "Missing: .Values.redisDatabase!" .Values.redisDatabase | quote }}
{{- if eq true .Values.redisAuthEnabled }}
    password = {{ .Values.redisPassword | quote }}
{{- end }}
  </session>
</redis>

<wopi-proxy>
    name          = wopi-proxy
    api_key       = {{ required "Missing: .Values.wopiApiKey!"  .Values.wopiApiKey | quote }}
    discovery_url = {{ required "Missing: .Values.wopiDiscoveryUrl!" .Values.wopiDiscoveryUrl | quote }}
    dev_mode      = {{ required "Missing: .Values.devMode!" .Values.devMode }}
</wopi-proxy>

<statsd>
    host = {{ include "wopi-proxy.statsd.host" . }}
    port = {{ include "wopi-proxy.statsd.port" . }}
</statsd>
{{- end -}}

{{- define "wopi-proxy.production.ini" -}}
{{- $httpConfig := .httpConfig -}}
[app:main]
use = egg:wopi_proxy

pyramid.reload_templates    = false
pyramid.debug_authorization = false
pyramid.debug_notfound      = false
pyramid.debug_routematch    = false
pyramid.default_locale_name = nl

minty_service.infrastructure.config_file = /etc/wopi-proxy/service.conf

{{/* Additional configuration settings for the app:main config */}}
{{- with $httpConfig.iniConfigApp }}
{{- range $idx, $map := . -}}
{{/* This heinous monstrosity is the result of https://github.com/helm/helm/issues/1707#issuecomment-268347183 */}}
{{- if eq (printf "%T" $map.value) "float64" -}}
{{ $map.name }} = {{ (printf "%.0f" $map.value) }}
{{ else -}}
{{ $map.name }} = {{ $map.value }}
{{ end -}}
{{- end -}}
{{- end }}

[server:main]
use = egg:waitress#main
listen = 0.0.0.0:6543

{{/* Additional configuration settings for the server:main config */}}
{{- with $httpConfig.iniConfigServer -}}
{{- range $idx, $map := . -}}
{{- if eq (printf "%T" $map.value) "float64" -}}
{{ $map.name }} = {{ (printf "%.0f" $map.value) }}
{{ else -}}
{{ $map.name }} = {{ $map.value }}
{{ end -}}
{{- end -}}
{{- end -}}
{{- end -}}
