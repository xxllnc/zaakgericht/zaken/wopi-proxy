# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
{{ if .Values.enabled -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "wopi-proxy.fullname" . }}
  labels:
    {{- include "wopi-proxy.labels" . | nindent 4 }}
spec:
  replicas: {{ .Values.replicaCount }}
  strategy:
    {{- toYaml .Values.strategy | nindent 4 }}
  selector:
    matchLabels:
      {{- include "wopi-proxy.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        {{- include "wopi-proxy.selectorLabels" . | nindent 8 }}
      annotations:
        checksum/config: {{ include (print $.Template.BasePath "/configmap.yaml") . | sha256sum }}
        checksum/secret: {{ include (print $.Template.BasePath "/secret.yaml") . | sha256sum }}
        {{- range $key, $value := .Values.podAnnotations }}
        {{ $key }}: {{ $value | quote }}
        {{- end }}
    spec:
    {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
    {{- end }}
    {{- if .Values.podSecurityContext }}
      securityContext: {{ toYaml .Values.podSecurityContext | nindent 8 }}
    {{- end }}
    {{- if .Values.priorityClassName }}
      priorityClassName: {{ .Values.priorityClassName }}
    {{- end }}
      serviceAccount: {{ include "wopi-proxy.serviceAccountName" . }}
      serviceAccountName: {{ include "wopi-proxy.serviceAccountName" . }}
      containers:
      - name: {{ .Chart.Name }}
        image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
        imagePullPolicy: {{ .Values.image.pullPolicy }}
        command: ["pserve"]
        args: ["/etc/wopi-proxy/production.ini"]
        securityContext:
          capabilities:
            drop:
            - ALL
          readOnlyRootFilesystem: true
        ports:
        - name: http
          containerPort: 6543
          protocol: TCP
        resources:
          {{- toYaml .Values.resources | nindent 10 }}
        volumeMounts:
        - name: production
          mountPath: /etc/wopi-proxy/production.ini
          subPath: production.ini
          readOnly: true
        - name: service
          mountPath: /etc/wopi-proxy/service.conf
          subPath: service.conf
          readOnly: true
        - mountPath: /tmp
          name: tmp
      {{- include "statsd.container" . | nindent 6 }}
      volumes:
        {{- include "statsd.volume" . | nindent 8 }}
        - name: production
          configMap:
            defaultMode: 420
            name: {{ include "wopi-proxy.fullname" . }}-config
        - name: service
          secret:
            defaultMode: 420
            secretName: {{ include "wopi-proxy.fullname" . }}-secret
        - name: tmp
          emptyDir: {}

      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
    {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
    {{- end }}
    {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
    {{- end }}
{{- end -}}
