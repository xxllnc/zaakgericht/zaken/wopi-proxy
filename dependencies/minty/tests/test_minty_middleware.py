# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
from minty.cqrs import EventService, UserInfo
from minty.middleware import AmqpPublisherMiddleware
from unittest import mock
from uuid import uuid4


class TestAMQPPublisherMiddleWare:
    """Test the AmqpPublisherMiddleware"""

    def setup_method(self):
        self.exchange = "test_exchange"
        self.infrastructure_name = "amqp_test"
        self.publisher_name = "case_management"

        self.correlation_id = uuid4()
        self.domain = "test_domain"
        self.context = "test_context"
        self.user_uuid = uuid4()
        self.user_info = UserInfo(
            user_uuid=self.user_uuid, permissions={"admin": False}
        )

        self.event_service = EventService(
            self.correlation_id,
            self.domain,
            self.context,
            self.user_uuid,
            self.user_info,
        )

        publisher = AmqpPublisherMiddleware(
            publisher_name="case_management",
            infrastructure_name=self.infrastructure_name,
        )
        with mock.MagicMock() as mock_repo_factory:
            mock_repo_factory.infrastructure_factory.get_config.return_value = {
                "amqp_test": {"publish_settings": {"exchange": self.exchange}}
            }
            self.mock_publisher = publisher(
                repository_factory=mock_repo_factory,
                correlation_id=self.correlation_id,
                domain=self.domain,
                context=self.context,
                user_uuid=self.user_uuid,
                event_service=self.event_service,
            )

    @mock.patch("amqpstorm.Message", spec=True)
    def test_amqp_publisher_middleware(self, mock_message):
        def mock_func():
            pass

        self.mock_publisher.repository_factory.infrastructure_factory.get_infrastructure.return_value = (
            "infra"
        )

        event_ids = [uuid4(), uuid4()]
        self.event_service.log_event(
            "FakeEntity",
            event_ids[0],
            "EventName",
            {"this": "that"},
            entity_data={},
        )
        self.event_service.log_event(
            "FakeEntity",
            event_ids[1],
            "EventName",
            {"this": "that"},
            entity_data={},
        )

        self.mock_publisher(func=mock_func)

        self.mock_publisher.repository_factory.infrastructure_factory.get_infrastructure.assert_called_with(
            infrastructure_name=self.infrastructure_name,
            context="test_context",
        )
        event_data = []
        for event in self.event_service.event_list:
            event_json = json.dumps(
                {
                    "id": str(event.uuid),
                    "created_date": event.created_date.isoformat(),
                    "correlation_id": str(self.correlation_id),
                    "context": event.context,
                    "domain": event.domain,
                    "user_uuid": str(event.user_uuid),
                    "user_info": json.loads(self.user_info.json()),
                    "entity_type": event.entity_type,
                    "entity_id": str(event.entity_id),
                    "event_name": event.event_name,
                    "changes": event.changes,
                    "entity_data": event.entity_data,
                },
                sort_keys=True,
            )
            event_data.append(
                mock.call(
                    channel="infra",
                    body=event_json,
                    properties={"content_type": "application/json"},
                )
            )
            event_data.append(
                mock.call().publish(
                    routing_key="zsnl.v2.test_domain.FakeEntity.EventName",
                    exchange=self.exchange,
                )
            )

        (name, call1_args, call1_kwargs) = mock_message.create.mock_calls[2]

        assert mock_message.create.mock_calls[0] == event_data[0]
        assert (
            str(mock_message.create.mock_calls[1]) == "call().method.__str__()"
        )
        assert (
            str(mock_message.create.mock_calls[2])
            == "call().properties.__str__()"
        )
        assert mock_message.create.mock_calls[3] == event_data[1]
