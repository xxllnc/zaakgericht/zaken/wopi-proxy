.. SPDX-FileCopyrightText: 2020 Mintlab B.V.
..
.. SPDX-License-Identifier: EUPL-1.2

Contributors
------------
\{\{cookiecutter.full_name\}\} (\{\{cookiecutter.email\}\})
