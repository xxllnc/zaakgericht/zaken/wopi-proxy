# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

# AUTOGENERATED FILE - No need to edit
# GENERATED AT: 2020-11-12T20:32:57"
# Re-create by running 'generate-routes' command
# Run 'generate-routes --help' for more options

import webtest
from unittest import TestCase, mock
from wopi_proxy import main, views


class TestRoutes_apidocs(TestCase):
    def setUp(self):
        self.patched_apidocs = mock.patch(
            "wopi_proxy.views.apidocs", spec=views.apidocs
        )
        self.apidocs = self.patched_apidocs.start()

        settings = {
            "minty_service.infrastructure.config_file": "tests/data/config.conf"
        }
        app = main({}, **settings)
        self.mock_app = webtest.TestApp(app)

    def tearDown(self):
        self.patched_apidocs.stop()

    def test_routes_apidocs(self):
        self.apidocs.apidocs_fileserver.return_value = 200
        self.mock_app.get("/api/documentopenapi/{filename}", status=200)


class TestRoutes_wopi(TestCase):
    def setUp(self):
        self.patched_wopi = mock.patch(
            "wopi_proxy.views.wopi", spec=views.wopi
        )
        self.wopi = self.patched_wopi.start()

        settings = {
            "minty_service.infrastructure.config_file": "tests/data/config.conf"
        }
        app = main({}, **settings)
        self.mock_app = webtest.TestApp(app)

    def tearDown(self):
        self.patched_wopi.stop()

    def test_routes_wopi(self):
        self.wopi.edit_document.return_value = 200
        self.mock_app.post("/api/document/edit", status=200)
        self.wopi.check_file_info.return_value = 200
        self.mock_app.get("/wopi/files/{file_id}", status=200)
        self.wopi.lock_file.return_value = 200
        self.mock_app.post("/wopi/files/{file_id}", status=200)
        self.wopi.put_file.return_value = 200
        self.mock_app.post("/wopi/files/{file_id}/contents", status=200)
        self.wopi.get_file.return_value = 200
        self.mock_app.get("/wopi/files/{file_id}/contents", status=200)
        self.wopi.health_check.return_value = 200
        self.mock_app.get("/api/health_check", status=200)
